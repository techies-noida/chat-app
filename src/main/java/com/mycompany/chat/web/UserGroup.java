/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.web;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author abc
 */
@Document(collection="userGroup")
public class UserGroup {
    
   @Id
   private Integer groupId;
    
    private String name;
    
    private String desc;
    
    private HashMap<Integer, List<String>> members;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public HashMap<Integer, List<String>> getMembers() {
        return members;
    }

    public void setMembers(HashMap<Integer, List<String>> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "UserGroup{" + "groupId=" + groupId + ", name=" + name + ", desc=" + desc + ", members=" + members + '}';
    }

    public UserGroup(Integer groupId, String name, String desc, HashMap<Integer, List<String>> members) {
        this.groupId = groupId;
        this.name = name;
        this.desc = desc;
        this.members = members;
    }

    public UserGroup()
    {
        
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.groupId);
        return hash;
    }
}
