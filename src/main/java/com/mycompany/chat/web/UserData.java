/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.web;

import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author abc
 */
@Document(collection="user")
public class UserData {
  
     @Id
   private String udid;
    
   private Integer senderUserId;
   
   private Integer userId;
   
   private String screenName;
   
   private String eccId;
   
   private String messageDateTime;
   
   private String msgText;
   
   private Integer msgBurn;
   
   private Integer mimeType;
   
   private Integer messageStatus;
   
   private String messageId;
   
   private Boolean reply;
   
   private Integer messageType;
   
   private Integer playSound;
   
   private Boolean isGroupMessage;
   
   private Boolean hasCreateGroup;
   
   private Boolean isDeleteMemberGroup;
   
   private Boolean isAddMemberGroup;
   
   private Integer groupId;
   
   private String name;
   
   public UserData()
   {
       
   }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public Integer getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(Integer senderUserId) {
        this.senderUserId = senderUserId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getEccId() {
        return eccId;
    }

    public void setEccId(String eccId) {
        this.eccId = eccId;
    }

    public String getMessageDateTime() {
        return messageDateTime;
    }

    public void setMessageDateTime(String messageDateTime) {
        this.messageDateTime = messageDateTime;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public Integer getMsgBurn() {
        return msgBurn;
    }

    public void setMsgBurn(Integer msgBurn) {
        this.msgBurn = msgBurn;
    }

    public Integer getMimeType() {
        return mimeType;
    }

    public void setMimeType(Integer mimeType) {
        this.mimeType = mimeType;
    }

    public Integer getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(Integer messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Boolean getReply() {
        return reply;
    }

    public void setReply(Boolean reply) {
        this.reply = reply;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    public Integer getPlaySound() {
        return playSound;
    }

    public void setPlaySound(Integer playSound) {
        this.playSound = playSound;
    }

    public Boolean getIsGroupMessage() {
        return isGroupMessage;
    }

    public void setIsGroupMessage(Boolean isGroupMessage) {
        this.isGroupMessage = isGroupMessage;
    }

    public Boolean getHasCreateGroup() {
        return hasCreateGroup;
    }

    public void setHasCreateGroup(Boolean hasCreateGroup) {
        this.hasCreateGroup = hasCreateGroup;
    }

    public Boolean getIsDeleteMemberGroup() {
        return isDeleteMemberGroup;
    }

    public void setIsDeleteMemberGroup(Boolean isDeleteMemberGroup) {
        this.isDeleteMemberGroup = isDeleteMemberGroup;
    }

    public Boolean getIsAddMemberGroup() {
        return isAddMemberGroup;
    }

    public void setIsAddMemberGroup(Boolean isAddMemberGroup) {
        this.isAddMemberGroup = isAddMemberGroup;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserData{" + "udid=" + udid + ", senderUserId=" + senderUserId + ", userId=" + userId + ", screenName=" + screenName + ", eccId=" + eccId + ", messageDateTime=" + messageDateTime + ", msgText=" + msgText + ", msgBurn=" + msgBurn + ", mimeType=" + mimeType + ", messageStatus=" + messageStatus + ", messageId=" + messageId + ", reply=" + reply + ", messageType=" + messageType + ", playSound=" + playSound + ", isGroupMessage=" + isGroupMessage + ", hasCreateGroup=" + hasCreateGroup + ", isDeleteMemberGroup=" + isDeleteMemberGroup + ", isAddMemberGroup=" + isAddMemberGroup + ", groupId=" + groupId + ", name=" + name + '}';
    }

    public UserData(String udid, Integer senderUserId, Integer userId, String screenName, String eccId, String messageDateTime, String msgText, Integer msgBurn, Integer mimeType, Integer messageStatus, String messageId, Boolean reply, Integer messageType, Integer playSound, Boolean isGroupMessage, Boolean hasCreateGroup, Boolean isDeleteMemberGroup, Boolean isAddMemberGroup, Integer groupId, String name) {
        this.udid = udid;
        this.senderUserId = senderUserId;
        this.userId = userId;
        this.screenName = screenName;
        this.eccId = eccId;
        this.messageDateTime = messageDateTime;
        this.msgText = msgText;
        this.msgBurn = msgBurn;
        this.mimeType = mimeType;
        this.messageStatus = messageStatus;
        this.messageId = messageId;
        this.reply = reply;
        this.messageType = messageType;
        this.playSound = playSound;
        this.isGroupMessage = isGroupMessage;
        this.hasCreateGroup = hasCreateGroup;
        this.isDeleteMemberGroup = isDeleteMemberGroup;
        this.isAddMemberGroup = isAddMemberGroup;
        this.groupId = groupId;
        this.name = name;
    }
    
    

    
    
    
    

   
   
   
}
