/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.controller;

import com.mycompany.chat.server.GlobalVariable;
import com.mycompany.chat.server.UserAndSocket;
import com.mycompany.chat.web.UserData;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.PongMessage;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import com.mycompany.chat.repository.UserDataRepository;
import com.mycompany.chat.repository.UserGroupRepository;
import com.mycompany.chat.web.UserGroup;
import java.util.ArrayList;
import org.json.JSONArray;

/**
 *
 * @author abc
 */
@Component
@ServerEndpoint("/endpoint/{client_id}")
@EnableMongoRepositories(basePackages = "com.mycompany.chat.repository")
public class Server extends SpringBeanAutowiringSupport {
   
    @Autowired
    private UserDataRepository userRepository;
    
    @Autowired
    private UserGroupRepository userGroupRepository;
    
    static Map<String,Integer>  ping = new HashMap<>();
    static Map<String,Integer>  pong = new HashMap<>();
    
    public static final Logger logger = Logger.getLogger(Server.class.getName());
    public Server()
    {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        System.out.println("Chat Repository"+ userRepository);
        System.out.println("User Group Repository "+userGroupRepository);
       
    }
    @OnOpen
    public void onOpen(final Session session, @PathParam("client_id") Integer clientId) throws IOException {

        ping.put(session.getId(), 0);
        pong.put(session.getId(), 0);
        System.out.println("Opened:::::::::" + clientId);
        Boolean isalreadyconnected = false;
        /*Single chat ...*/

        UserAndSocket removeuserAndSocket = null;
//            for (UserAndSocket userAndSocket : GlobalVariable.userAndSocket) {
//                /*Has reciever is online*/
//                if (userAndSocket.getUserId().equals(clientId)) {
//                    isalreadyconnected = true;
//                    removeuserAndSocket = userAndSocket;
//                    break;
//                }
//            }
                if(GlobalVariable.user.containsKey(clientId)){
                    isalreadyconnected = true;
                }
        
        System.out.println("isalreadyconnected::::" + isalreadyconnected);
        if (removeuserAndSocket != null) {
            try {
                GlobalVariable.user.remove(clientId);
                removeuserAndSocket.getTimer().cancel();
                ping.remove(removeuserAndSocket.getSocket().getId());
                pong.remove(removeuserAndSocket.getSocket().getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        UserAndSocket userAndSocket = new UserAndSocket();
        userAndSocket.setSocket(session);
        userAndSocket.setUserId(clientId);

        sendBufferData(session, clientId);

        MessageHandler.Whole<PongMessage> whole = new MessageHandler.Whole<PongMessage>() {

            @Override
            public void onMessage(PongMessage pongMessage) {
               // System.out.println("pongMessage" + new String(pongMessage.getApplicationData().array()));
                Integer pcount = pong.get(session.getId());
                if (pcount != null) {
                    pcount++;
                    pong.put(session.getId(), pcount);
                }
            }
        };
        MessageHandler.Whole<ByteBuffer> whole1 = new MessageHandler.Whole<ByteBuffer>() {

            @Override
            public void onMessage(ByteBuffer pongMessage) {

                try {
                    Integer userId = Integer.parseInt(new String(pongMessage.array()));
                    Boolean isalreadyconnected = false;
                    /*Single chat ...*/
//                        for (UserAndSocket userAndSocket : GlobalVariable.userAndSocket) {
//                            /*Has reciever is online*/
//                            if (userAndSocket.getUserId().equals(userId)) {
//                                isalreadyconnected = true;
//                                break;
//                            }
//                        }
                            if(GlobalVariable.user.containsKey(userId)){
                                isalreadyconnected = true;
                                
                            }
                   
                    System.out.println("isalreadyconnected:" + isalreadyconnected);
                    if (!isalreadyconnected) {
                        sendBufferData(session, userId);
                        ping.put(session.getId(), 0);
                        pong.put(session.getId(), 0);
                        UserAndSocket userAndSocket = new UserAndSocket();
                        userAndSocket.setSocket(session);
                        userAndSocket.setUserId(userId);
                        System.out.println("asdasdasdsdasdasdasd" + session.getMessageHandlers().size());
                        if (session.getMessageHandlers().isEmpty()) {
                            MessageHandler.Whole<PongMessage> whole = new MessageHandler.Whole<PongMessage>() {
                                @Override
                                public void onMessage(PongMessage pongMessage) {
                                    Integer pcount = pong.get(session.getId());
                                    if (pcount != null) {
                                        pcount++;
                                        pong.put(session.getId(), pcount);
                                    }
                                }
                            };
                            session.addMessageHandler(whole);
                        }
                        Timer startConnectionLostTimer = startConnectionLostTimer(session,userId);
                        userAndSocket.setTimer(startConnectionLostTimer);

                        //GlobalVariable.userAndSocket.add(userAndSocket);
                        GlobalVariable.user.put(userId, userAndSocket);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        session.addMessageHandler(whole);
        session.addMessageHandler(whole1);

        Timer startConnectionLostTimer = startConnectionLostTimer(session,clientId);
        userAndSocket.setTimer(startConnectionLostTimer);
        //GlobalVariable.userAndSocket.add(userAndSocket);
        GlobalVariable.user.put(clientId, userAndSocket);
        System.out.println("new global "+GlobalVariable.user);

    }

    
    
    @OnMessage
    public String onMessageString(String message,final Session session) 
    {      
        try {
            
                System.out.println("clentjson : " + message);
                JSONObject jsonObject = new JSONObject(message);
                Integer requestId =(Integer) jsonObject.get("requestId");
                
                switch(requestId)
                {
                    //Single Chat
                    case 1:{
                        Integer userId =(Integer) jsonObject.get("sendFrom");
                        String screenName=(String) jsonObject.get("screenName");
                        String eccId =(String) jsonObject.get("eccId");
                        Integer userSendToId =(Integer) jsonObject.get("sendTo");
                        String msgDateTime =(String) jsonObject.get("msgDateTime");
                        String msgText =(String) jsonObject.get("msgText");
                        Integer msgBurn =(Integer) jsonObject.get("msgBurn");
                        Integer mimeType =(Integer)jsonObject.get("mimeType");
                        String messageId =(String) jsonObject.get("messageId");
                        System.out.println("sendFrom: "+userId);

                        System.out.println("repository "+userRepository);
                        userRepository.deleteUserDataByMessageId(messageId, userSendToId);
                        System.out.println("after clearing "+userRepository);
                       
                        Boolean isalreadyconnected = false;
                            /*Single chat ...*/
                           UserAndSocket removeuserAndSocket = null;
//                        for (UserAndSocket userAndSocket : GlobalVariable.userAndSocket) {
//                            /*Has reciever is online*/
//                            if (userAndSocket.getUserId().equals(userId)) {
//                                isalreadyconnected = true;
//                                removeuserAndSocket = userAndSocket;
//                                break;
//                            }
//                        }
                            if(GlobalVariable.user.containsKey(userId)){
                                isalreadyconnected = true;
                            }
                                            
                    if (!isalreadyconnected) {
                        sendBufferData(session, userId);
                        ping.put(session.getId(), 0);
                        pong.put(session.getId(), 0);
                        UserAndSocket userAndSocket = new UserAndSocket();
                        userAndSocket.setSocket(session);
                        userAndSocket.setUserId(userId);
                      
                        System.out.println("asdasdasdsdasdasdasd" + session.getMessageHandlers().size());
                        if (session.getMessageHandlers().isEmpty()) {
                            MessageHandler.Whole<PongMessage> whole = new MessageHandler.Whole<PongMessage>() {
                                @Override
                                public void onMessage(PongMessage pongMessage) {
                                    Integer pcount = pong.get(session.getId());
                                    if (pcount != null) {
                                        pcount++;
                                        pong.put(session.getId(), pcount);
                                    }
                                }
                            };
                            session.addMessageHandler(whole);
                        }
                        Timer startConnectionLostTimer = startConnectionLostTimer(session,userId);
                        userAndSocket.setTimer(startConnectionLostTimer);

                        //GlobalVariable.userAndSocket.add(userAndSocket);
                        GlobalVariable.user.put(userId, userAndSocket);
                    }
                        boolean isReceiverOnline = false;
                       
                            //isReceiverOnline
//                            for(UserAndSocket userAndSocket: GlobalVariable.userAndSocket){
//                                if(userAndSocket.getUserId().equals(userSendToId)){
//                                     isReceiverOnline = true;                               
//                                    int sound =0;
//                                try{
//                                      userAndSocket.getSocket().getAsyncRemote().sendText("{\"response\":2,\"sendFrom\":" +userId + ",\"screenName\":"+ screenName + ",\"eccId\":" + eccId +  ",\"sendTo\":" + userSendToId + ",\"msgDateTime\":" + msgDateTime + ",\"msgText\":" +msgText + ",\"msgVBurn\":" + msgBurn +  ",\"playSound\":" + sound  + ",\"mimeType\":" + mimeType + ",\"messageStatus\":" + 0 + ",\"messageId\":" + messageId + "\",\"reply\":\"true\"" + "\"}");
//
//                                }catch(Exception e){
//                                    e.printStackTrace();
//                                    }
//                                }
//                            }
                            System.out.println("hellllooo");
                            if(GlobalVariable.user.containsKey(userSendToId)){
                            UserAndSocket userAndSocket = GlobalVariable.user.get(userSendToId);
                            System.out.println("get data "+userAndSocket);
                            isReceiverOnline = true;                               
                            int sound =0;
                            try{
                                userAndSocket.getSocket().getAsyncRemote().sendText("{\"response\":2,\"sendFrom\":" +userId + ",\"screenName\":"+ screenName + ",\"eccId\":" + eccId +  ",\"sendTo\":" + userSendToId + ",\"msgDateTime\":" + msgDateTime + ",\"msgText\":" +msgText + ",\"msgVBurn\":" + msgBurn +  ",\"playSound\":" + sound  + ",\"mimeType\":" + mimeType + ",\"messageStatus\":" + 0 + ",\"messageId\":" + messageId + "\",\"reply\":\"true\"" + "\"}");

                             }catch(Exception e){
                                    e.printStackTrace();
                              }
                            }
                     
                                                       
                        //save data in buffer
                       UserData userData = new UserData();
                        
                        userData.setUserId(userSendToId);
                        System.out.println("send to "+userSendToId);
                        userData.setScreenName(screenName);
                        userData.setEccId(eccId);
                        userData.setSenderUserId(userId);
                        userData.setMessageDateTime(msgDateTime);
                        userData.setMsgText(msgText);
                        userData.setMsgBurn(msgBurn);
                        userData.setMimeType(mimeType);
                        userData.setMessageId(messageId);
                        userData.setReply(true);
                        userData.setIsGroupMessage(false);
                        userData.setHasCreateGroup(false);
                        userData.setIsDeleteMemberGroup(false);
                        userData.setIsAddMemberGroup(false);
                        userData.setMessageType(1);
                        
                        System.out.println("Data is "+userData);
                        userRepository.save(userData);
                        System.out.println("Chat saved successfully");
                        //Offline
                        if(!isReceiverOnline)
                        {
                          return "{\"response\":1,\"msgstatus\":\"Receiver is not online\",\"messageId\":\"" + messageId + "\"}";
                        }
                     return "{\"response\" :1,\"msgStatus\":\"Sent\",\"messageId\":\"" + messageId + "\"}";    
                    }
                    
                    //Create Group
                    case 2:{
                        
                        HashMap<Integer,List<String>> membersdata = new HashMap<>();
                        Integer groupId = (Integer) jsonObject.get("groupId");
                        Integer creatorId =(Integer) jsonObject.get("creatorId");
                        String name =(String) jsonObject.get("name");
                        String eccId = (String) jsonObject.get("eccId");
                        String screenName =(String) jsonObject.get("screenName");
                        String messageId ="999"+groupId;
                        JSONArray membersarray =(JSONArray)jsonObject.get("members");
                        for(int i=0;i<membersarray.length();i++){
                           
                            JSONObject childData = (JSONObject) membersarray.get(i);
                            Integer memberId =(Integer) childData.get("userId");
                            String eccid =(String) childData.get("eccId");
                            String screenname =(String) childData.get("screenName");
                            
                            List<String> userInfo = new ArrayList();
                            userInfo.add(eccid);
                            userInfo.add(screenname);
                            membersdata.put(memberId, userInfo);
                        }
                        
                        List<String> userInfoNew = new ArrayList();
                        userInfoNew.add(eccId);
                        userInfoNew.add(screenName);
                        membersdata.put(creatorId, userInfoNew);
                        
                      UserGroup userGroup=  userGroupRepository.findUserGroupByGroupId(groupId);
                        if(userGroup==null){
                            userGroup = new UserGroup();
                            userGroup.setGroupId(groupId);
                            userGroup.setMembers(membersdata);
                            userGroup.setName(name);
                            userGroupRepository.save(userGroup);
                        }
                        
                        System.out.println("............Group Create..........." + userGroup);
                        for(Map.Entry<Integer,List<String>> entry: membersdata.entrySet()){
                            Integer memberId = entry.getKey();
                            Boolean isROnline = false;
                            
//                            for(UserAndSocket userAndSocket: GlobalVariable.userAndSocket){
//                                if(userAndSocket.getUserId().equals(memberId) && !userAndSocket.getUserId().equals(creatorId)){
//                                   System.out.println("............Data sending to members..........." + memberId);
//                                    isROnline = true;
//                                    userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":9,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId + ",\"eccId\":\"" + eccId + "\",\"screenName\":\"" + screenName + "\",\"name\":\"" + name + "\",\"members\":" + membersarray.toString() + ",\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgTex\":\"" + "" + "\"}");
// 
//                                }
//                            }
                               if(GlobalVariable.user.containsKey(memberId) && !GlobalVariable.user.containsKey(creatorId)){
                                   UserAndSocket userAndSocket = GlobalVariable.user.get(memberId);
                                   System.out.println("............Data sending to members..........." + memberId);
                                   isROnline = true;
                                   userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":9,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId + ",\"eccId\":\"" + eccId + "\",\"screenName\":\"" + screenName + "\",\"name\":\"" + name + "\",\"members\":" + membersarray.toString() + ",\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgTex\":\"" + "" + "\"}");

                               }
                            
                            if(!isROnline && !memberId.equals(creatorId)){
                                
                                //save data in buffer
                                UserData userData = new UserData();
                                
                                userData.setUserId(memberId);
                                userData.setSenderUserId(creatorId);
                                userData.setGroupId(groupId);
                                userData.setName(name);
                                userData.setEccId(eccId);
                                userData.setScreenName(screenName);
                                userData.setMessageId(messageId);
                                userData.setMsgText(membersarray.toString());
                                userData.setIsGroupMessage(true);
                                userData.setHasCreateGroup(true);
                                userData.setIsAddMemberGroup(false);
                                userData.setIsDeleteMemberGroup(false);
                                userData.setMessageType(2);
                                
                                userRepository.save(userData);
                                System.out.println("............Data sending to members whose offline..........." + memberId);       
                            }
                        }

                        return "{\"response\":3,\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgstatus\":\"Group has been successfully created\"}";

                    }
                    
                    case 3:{  //Add members in Group
                        
                        HashMap<Integer,List<String>> membersdata = new HashMap<>();
                        Integer groupId = (Integer) jsonObject.get("groupId");
                        Integer creatorId = (Integer) jsonObject.get("creatorId");
                        String name = (String) jsonObject.get("name");
                        String eccId =(String) jsonObject.get("eccId");
                        String screenName = (String) jsonObject.get("screenName");
                        String messageId = "123456789"+ groupId;
                        JSONArray membersarray = (JSONArray) jsonObject.get("members");
                        for(int i =0;i<membersarray.length();i++){
                            
                            JSONObject childData = (JSONObject) membersarray.get(i);
                            Integer memberId = (Integer) childData.get("userId");
                            String eccid =(String) childData.get("eccId");
                            String screenname =(String) childData.get("screenName");
                            String eccKey =(String) childData.get("eccKey");
                            
                            List<String> userInfo = new ArrayList<>();
                            userInfo.add(eccid);
                            userInfo.add(screenname);
                            userInfo.add(eccKey);
                            membersdata.put(memberId, userInfo);
                        }
                        

                        UserGroup userGroup = userGroupRepository.findUserGroupByGroupId(groupId);
                        System.out.println("user group "+ userGroup);
                        if(userGroup!=null){
                            if(userGroup.getGroupId().equals(groupId)){
                               HashMap<Integer,List<String>> ugmembers = userGroup.getMembers();
                               System.out.println("before adding "+ugmembers.size());
                               ugmembers.putAll(membersdata);
                               userGroup.setMembers(ugmembers);
                               System.out.println("after adding "+ugmembers.size());
                               userGroupRepository.save(userGroup);
                               
                                                   

                            HashMap<Integer,List<String>> members = userGroup.getMembers();
                            for(Map.Entry<Integer,List<String>> entry: members.entrySet()){
                            
                            Integer memberId = entry.getKey();
                            Boolean isROnline = false;
//                            for(UserAndSocket userAndSocket: GlobalVariable.userAndSocket){
//                                if(userAndSocket.getUserId().equals(memberId) && !userAndSocket.getUserId().equals(creatorId)){
//                                  System.out.println("............Data sending to members..........." + memberId);
//                                  isROnline = true;
//                                   try{
//                                       userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":22,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId + ",\"eccId\":\"" + eccId + "\",\"name\":\"" + name + "\",\"screenName\":\"" + screenName + "\",\"members\":" + membersarray + ",\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgTex\":\"" + "" + "\"}");
//                                }catch(Exception e){
//                                    e.printStackTrace();
//                                   }     
//                                }
//                            }
                              if(GlobalVariable.user.containsKey(memberId) && ! GlobalVariable.user.containsKey(creatorId)){
                                  UserAndSocket userAndSocket = GlobalVariable.user.get(memberId);
                                  System.out.println("............Data sending to members..........." + memberId);
                                  isROnline = true;
                                   try{
                                       userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":22,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId + ",\"eccId\":\"" + eccId + "\",\"name\":\"" + name + "\",\"screenName\":\"" + screenName + "\",\"members\":" + membersarray + ",\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgTex\":\"" + "" + "\"}");
                                }catch(Exception e){
                                    e.printStackTrace();
                                   }  
                              }                   
                            if(!isROnline && !memberId.equals(creatorId)){
                                
                                UserData userData = new UserData();
                                
                                userData.setUserId(memberId);
                                userData.setSenderUserId(creatorId);
                                userData.setEccId(eccId);
                                userData.setScreenName(screenName);
                                userData.setName(name);
                                userData.setGroupId(groupId);
                                userData.setMessageId(messageId);
                                userData.setMsgText(membersarray.toString());
                                userData.setHasCreateGroup(false);
                                userData.setIsGroupMessage(true);
                                userData.setIsAddMemberGroup(true);
                                userData.setIsDeleteMemberGroup(false);
                                userData.setMessageType(3);
                                
                                userRepository.save(userData);
                                System.out.println("............Data sending to members whose offline..........." + memberId);      
                            }
                        }
                        return "{\"response\":5,\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgstatus\":\"Members has been successfully added\"}";
                     } 
                  }
                  else{
                        return "{\"response\":5,\"messageId\":\"" + messageId + "\",\"messsagestatus\":" + 0 + ",\"msgstatus\":\"Group Info does not exists\"}";
                      }       
                    }
                    
                case 4:{ //Delete member from Group
                    
                    Integer creatorId = (Integer) jsonObject.get("creatorId");                  
                    Integer groupId = (Integer) jsonObject.get("groupId");
                    String eccId = (String) jsonObject.get("eccId");
                    String screenName =(String) jsonObject.get("screenName");
                    String name = (String) jsonObject.get("name");
                    JSONArray membersarray = (JSONArray) jsonObject.get("members");
                    String messageId = "55"+groupId;
                    
                    UserGroup userGroup = userGroupRepository.findUserGroupByGroupId(groupId);
                    System.out.println("User Group "+ userGroup);
                    if(userGroup!= null && userGroup.getGroupId().equals(groupId)){
                        
                        HashMap<Integer,List<String>> ugmembers = userGroup.getMembers();
                        HashMap<Integer,List<String>> allmembers = new HashMap<>();
                        allmembers.putAll(ugmembers);
                        System.out.println("before removing "+ugmembers.size());
                        for(int i=0;i<membersarray.length();i++){
                            
                            JSONObject child = (JSONObject) membersarray.get(i);
                            Integer memberId = (Integer) child.get("userId");
                            ugmembers.remove(memberId);
                        }
                        userGroup.setMembers(ugmembers);
                        System.out.println("after removing "+ugmembers.size());
                        userGroupRepository.save(userGroup);
                        
                        /*Delete members..*/
                        for(Map.Entry<Integer,List<String>> entry: allmembers.entrySet()){
                            Integer memberId = entry.getKey();
                            Boolean isROnline = false;
//                            for(UserAndSocket userAndSocket: GlobalVariable.userAndSocket){
//                                
//                                if(userAndSocket.getUserId().equals(memberId) && !userAndSocket.getUserId().equals(creatorId)){
//                                   System.out.println("............Data sending to members..........." + memberId);
//                                   isROnline = true;
//                                   try{
//                                     userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":33,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId  + ",\"eccId\":\"" + eccId + "\",\"name\":\"" + name + "\",\"screenName\":\"" + screenName + "\",\"members\":" + membersarray.toString() + ",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgText\":\"" + "" + "\"}");
//                                   }catch(Exception e){
//                                       e.printStackTrace();
//                                   }
//                                   break;
//                                }
//                            }
                            if(GlobalVariable.user.containsKey(memberId) && ! GlobalVariable.user.containsKey(creatorId)){
                                UserAndSocket userAndSocket = GlobalVariable.user.get(memberId);
                                System.out.println("............Data sending to members..........." + memberId);
                                   isROnline = true;
                                   try{
                                     userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":33,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId  + ",\"eccId\":\"" + eccId + "\",\"name\":\"" + name + "\",\"screenName\":\"" + screenName + "\",\"members\":" + membersarray.toString() + ",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgText\":\"" + "" + "\"}");
                                   }catch(Exception e){
                                       e.printStackTrace();
                                   }
                                   break;
                            }
                            if(!isROnline && !memberId.equals(creatorId)){
                               
                                //saveData
                                UserData userData = new UserData();
                                userData.setUserId(memberId);
                                userData.setSenderUserId(creatorId);
                                userData.setGroupId(groupId);
                                userData.setEccId(eccId);
                                userData.setScreenName(screenName);
                                userData.setMessageId(messageId);
                                userData.setName(name);
                                userData.setHasCreateGroup(false);
                                userData.setIsAddMemberGroup(false);
                                userData.setIsDeleteMemberGroup(true);
                                userData.setIsGroupMessage(true);
                                userData.setMsgText(membersarray.toString());
                                userData.setMessageType(4);
                                
                                userRepository.save(userData);
                               System.out.println("Data sending to memebers whose offline "+ memberId);
                            }
                        }
                        return ("{\"response\": 6 ,\"messageId\":\"" + messageId + "\",\"messagestatus\":"+ 0 + ",\"msgstatus\":\"Members has been deleted successfully\"}");
                    }else
                        return ("{\"response\":6,\"messageId\":\"" + messageId + "\",\"messagestaus\":" + 0 + ",\"msgstatus\":\"Group Info does not exist\"}");
                     
                    }
                
                case 5:{  // Send Group Message
                    
                        Integer groupId = (Integer) jsonObject.get("groupId");
                        Integer senderId =(Integer) jsonObject.get("senderId");
                        String msgDateTime = (String) jsonObject.get("msgDateTime");
                        String msgText = (String) jsonObject.get("msgText");
                        String eccId = (String) jsonObject.get("eccId");
                        String screenName = (String) jsonObject.get("screenName");
                        Integer msgBurn = (Integer) jsonObject.get("msgBurn");
                        Integer mimeType =(Integer) jsonObject.get("mimeType");
                        String messageId = "832"+groupId;
                        
                        
                        UserGroup userGroup = userGroupRepository.findUserGroupByGroupId(groupId);
                        System.out.println("User Group data "+ userGroup);
                        if(userGroup!= null && userGroup.getGroupId().equals(groupId)){
                            
                            HashMap<Integer,List<String>> members = userGroup.getMembers();
                            System.out.println("Members are :::: "+ members);
                            for(Map.Entry<Integer,List<String>> entry : members.entrySet()){
                               Boolean isROnline = false;
                               Integer memberId = entry.getKey();
                               
//                               for(UserAndSocket userAndSocket : GlobalVariable.userAndSocket){
//                                   if(userAndSocket.getUserId().equals(memberId) && ! userAndSocket.getUserId().equals(senderId)){
//                                       isROnline =true;
//                                       System.out.println("Sending to memebers:::: "+memberId);
//                                       try{
//                                          userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":8,\"groupId\":" + groupId + ",\"senderId\":" + senderId + ",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"eccId\":\"" + eccId + "\",\"screenName\":\"" + screenName + "\",\"mimeType\":" + mimeType + ",\"playSound\":" + 0 + ",\"msgBurn\":" + msgBurn + ",\"msgDateTime\":\"" + msgDateTime + "\",\"msgText\":\""+ msgText + "\"}");
//                                       }catch(Exception e){
//                                           e.printStackTrace();
//                                       }
//                                       break;
//                                   }
//                               }
                                if(GlobalVariable.user.containsKey(memberId) && !GlobalVariable.user.containsKey(senderId)){
                                    UserAndSocket userAndSocket = GlobalVariable.user.get(memberId);
                                    isROnline =true;
                                    System.out.println("Sending to memebers:::: "+memberId);
                                    try{
                                         userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":8,\"groupId\":" + groupId + ",\"senderId\":" + senderId + ",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"eccId\":\"" + eccId + "\",\"screenName\":\"" + screenName + "\",\"mimeType\":" + mimeType + ",\"playSound\":" + 0 + ",\"msgBurn\":" + msgBurn + ",\"msgDateTime\":\"" + msgDateTime + "\",\"msgText\":\""+ msgText + "\"}");
                                       }catch(Exception e){
                                           e.printStackTrace();
                                       }
                                       break;
                                }
                               if(!isROnline && ! memberId.equals(senderId)){
                                   
                                   UserData userData = new UserData();
                                   userData.setSenderUserId(senderId);
                                   userData.setUserId(memberId);
                                   userData.setGroupId(groupId);
                                   userData.setEccId(eccId);
                                   userData.setScreenName(screenName);
                                   userData.setMsgBurn(msgBurn);
                                   userData.setMimeType(mimeType);
                                   userData.setMessageDateTime(msgDateTime);
                                   userData.setMessageId(messageId);
                                   userData.setMsgText(msgText);
                                   userData.setHasCreateGroup(false);
                                   userData.setIsAddMemberGroup(false);
                                   userData.setIsDeleteMemberGroup(false);
                                   userData.setIsGroupMessage(true);
                                   userData.setMessageType(5);
                                   
                                   userRepository.save(userData);
                               System.out.println("Data sending to memebers whose offline "+ memberId);

                               }
                            }
                            return ("{\"response\":7,\"groupId\":" + groupId + ",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgstatus\":\"SENT\"}");
                        }
                        else{
                            return ("{\"response\":7,\"groupId\":" + groupId + ",\"messageId\":\""+ messageId + "\",\"messagestatus\":" + 0 + ",\"msgstatus\":\"CAN NOT SENT GROUP NOT FOUND\"}");
                        }             
                    }
                
                case 6:{   //Leave member from Group
                        
                        Integer groupId = (Integer) jsonObject.get("groupId");
                        Integer creatorId = (Integer) jsonObject.get("creatorId");
                        String eccId = (String) jsonObject.get("eccId");
                        String screenName = (String) jsonObject.get("screenName");
                        String messageId = "789"+groupId;
                        String name = (String) jsonObject.get("name");
                        JSONArray membersarray = (JSONArray) jsonObject.get("members");
                        
                        UserGroup userGroup = userGroupRepository.findUserGroupByGroupId(groupId);
                        if(userGroup!=null && userGroup.getGroupId().equals(groupId)){
                            
                            HashMap<Integer,List<String>> ugmembers = userGroup.getMembers();
                            System.out.println("before leaving "+ ugmembers.size());
                            HashMap<Integer,List<String>> allmembers = new HashMap<>();
                            allmembers.putAll(ugmembers);
                            
                            for(int i =0;i<membersarray.length();i++){
                                JSONObject childData = (JSONObject) membersarray.get(i);
                                Integer memberId = (Integer) childData.get("userId");
                                ugmembers.remove(memberId);
                            }
                            userGroup.setMembers(ugmembers);
                            System.out.println("After leaving "+ ugmembers.size());
                            userGroupRepository.save(userGroup);
                            
                            for(Map.Entry<Integer,List<String>> entry: allmembers.entrySet()){
                                Integer memberId = entry.getKey();
                                Boolean isROnline = false;
                                
                                if(GlobalVariable.user.containsKey(memberId) && ! GlobalVariable.user.containsKey(creatorId)){
                                    
                                    UserAndSocket userAndSocket = GlobalVariable.user.get(memberId);
                                    isROnline = true;
                                    System.out.println("....Data sending to members......"+ memberId);
                                    try{
                                        userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":77,\"groupId\":" + groupId + ",\"creatorId\":" + creatorId + ",\"eccId\":\"" + eccId + "\",\"screenName\":\"" + screenName + "\",\"name\":\"" + name + "\",\"members\":\"" + membersarray.toString() + "\",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgText\":\"" + "" + "\"}");
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
                                }
                                
                                if(!isROnline && !memberId.equals(creatorId)){
                                    
                                    UserData userData = new UserData();
                                    userData.setSenderUserId(creatorId);
                                    userData.setUserId(memberId);
                                    userData.setGroupId(groupId);
                                    userData.setEccId(eccId);
                                    userData.setScreenName(screenName);
                                    userData.setMessageId(messageId);
                                    userData.setName(name);
                                    userData.setHasCreateGroup(false);
                                    userData.setIsGroupMessage(true);
                                    userData.setIsAddMemberGroup(true);
                                    userData.setIsDeleteMemberGroup(false);
                                    userData.setMsgText(membersarray.toString());
                                    userData.setMessageType(6);
                                    
                                    userRepository.save(userData);
                                    System.out.println(".....Data sending to member whose offline......"+ memberId);
                                }       
                            }
                            return "{\"response\":16,\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgstatus\":\"Member has been successfully left\"}";
                        }
                        else{
                            return "{\"response\":16,\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgstatus\":\"Group Info does nor exist\"}";
                        }  
                    }
                
                case 7:{    //Update Group Name
                        
                            Integer groupId = (Integer) jsonObject.get("groupId");
                            String name = (String) jsonObject.get("name");
                            Integer updatorId = (Integer) jsonObject.get("updatorId");
                            String messageId = "1123"+groupId;
                            
                            UserGroup userGroup = userGroupRepository.findUserGroupByGroupId(groupId);
                            if(userGroup!=null && userGroup.getGroupId().equals(groupId)){
                                
                                
                                HashMap<Integer,List<String>> members = userGroup.getMembers();
                                for(Map.Entry<Integer,List<String>> entry : members.entrySet()){
                                    
                                    
                                    Integer memberId = entry.getKey();
                                    Boolean isROnline = false;
                                    
                                    if(GlobalVariable.user.containsKey(memberId) && ! GlobalVariable.user.containsKey(updatorId)){
                                        System.out.println("....Data sending to member..."+memberId);
                                        UserAndSocket userAndSocket = GlobalVariable.user.get(memberId);
                                        isROnline = true;
                                        try{
                                            userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":99,\"groupId\":" + groupId + ",\"updatorId\":" + updatorId + ",\"updateName\":\"" + name + "\",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgtext\":\"" + "" +"\"}");
                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }  
                                    }
                                    
                                    if(!isROnline && ! memberId.equals(updatorId)){
                                        
                                        UserData userData = new UserData();
                                        
                                        userData.setSenderUserId(updatorId);
                                        userData.setUserId(memberId);
                                        userData.setGroupId(groupId);
                                        userData.setName(name);
                                        userData.setIsGroupMessage(true);
                                        userData.setIsAddMemberGroup(true);
                                        userData.setIsDeleteMemberGroup(false);
                                        userData.setMessageId(messageId);
                                        userData.setMessageType(7);
                                        userRepository.save(userData);
                                        System.out.println("..Data sending to memeber whose offline..."+memberId);
                                    }
                                    
                                }
                                userGroup.setName(name);
                                userGroupRepository.save(userGroup);
                                return "{\"response\":41,\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgstatus\":\"Group Name has been successfully updated\"}";
                            }
                            else{
                                return "{\"response\":41,\"messageId\":\"" + messageId + "\",\"messagestatus\":" + 0 + ",\"msgstatus\":\"Group Info does not exist\"}" ;
                            }
                    }
                
                case 8 :{  // Acknowledgement of Messages
                        
                        Integer userId = (Integer) jsonObject.get("sendFrom");
                        Integer userSendToId =(Integer) jsonObject.get("sendTo");
                        String eccId =(String) jsonObject.get("eccId");
                        String screenName = (String) jsonObject.get("screenName");
                        Integer messagestatus =(Integer) jsonObject.get("messagestatus");
                        String messageId =(String) jsonObject.get("messageId");
                        
                        Boolean isROnline = false;
                        System.out.println("Before repository "+ userRepository);
                        userRepository.deleteDataByMessageId(messageId);
                        System.out.println("After clearing "+userRepository);
                        
                        if(GlobalVariable.user.containsKey(userSendToId)){
                            UserAndSocket userAndSocket = GlobalVariable.user.get(userSendToId);
                            isROnline = true;
                            try{
                                 userAndSocket.getSocket().getBasicRemote().sendText("{\"response\":20,\"sendFrom\":" + userId + ",\"sendTo\":" + userSendToId + ",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + messagestatus + ",\"eccId\":\"" + eccId + "\",\"screenName\":\"" + screenName + "\",\"reply\":\"" + "false" + "\"}" );
                            }catch(Exception e){
                                if(messagestatus.equals(3)){
                                    userRepository.deleteDataByMessageId(messageId);
                                }
                            }
                        }
                        if(!isROnline){
                            if(messagestatus.equals(3)){
                                userRepository.deleteDataByMessageId(messageId);
                            }
                                UserData userData = new UserData();
                                userData.setUserId(userSendToId);
                                userData.setSenderUserId(userId);
                                userData.setEccId(eccId);
                                userData.setScreenName(screenName);
                                userData.setMessageId(messageId);
                                userData.setMessageStatus(messagestatus);
                                userData.setHasCreateGroup(false);
                                userData.setIsAddMemberGroup(false);
                                userData.setIsDeleteMemberGroup(false);
                                userData.setIsGroupMessage(false);
                                userData.setReply(false);
                                userData.setMsgText("");
                                userData.setMessageType(8); 
                                userRepository.save(userData); 
                                
                                return "{\"response\":71,\"deliverystatus\":\"0\",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + messagestatus + "}";
                        }
                        return "{\"response\":71,\"deliverystatus\":\"1\",\"messageId\":\"" + messageId + "\",\"messagestatus\":" + messagestatus + "}";   
                    }
                   
                
                } 
        }catch (Exception e) {
            e.printStackTrace();
        }
     return "";   
    }   
    
    @OnClose
    public void onClose(Session session, @PathParam("client_id") Integer clientId) {
        System.out.println("clientId::::" + clientId);
        UserAndSocket andSocket = null;
//            for (UserAndSocket userAndSocket : GlobalVariable.userAndSocket) {
//                if (userAndSocket.getUserId().equals(clientId)) {
//                    System.out.println("GlobalVar.userAndSockets::::::On close" + userAndSocket);
//                    andSocket = userAndSocket;
//                    break;
//                }
//            }
            if(GlobalVariable.user.containsKey(clientId)){
                UserAndSocket userAndSocket = GlobalVariable.user.get(clientId);
                System.out.println("GlobalVar.userSocket::::::On close" + userAndSocket);
                andSocket = userAndSocket;

            }
            System.out.println("Socket data ..."+andSocket);
            
            if(andSocket!=null){
                GlobalVariable.user.remove(clientId);
                andSocket.getTimer().cancel();
            }    
        

        ping.remove(session.getId());
        pong.remove(session.getId());
        System.out.println("Global var.... "+GlobalVariable.user);
        
    }
    
   @OnError
    public void onError(Throwable t) {
        System.out.println("onError::" + t.getMessage());
    }
    
   protected Timer startConnectionLostTimer(final Session session, Integer clientId) {
        Timer connectionLostTimer1 = new Timer();
        TimerTask connectionLostTimerTask1 = new TimerTask() {

            /**
             * Keep the connections in a separate list to not cause deadlocks
             */
            @Override
            public void run() {
//                System.out.println("After one sec - pingcount" + pingcount);
//                System.out.println("After one sec - pongcount" + pongcount);
                Integer pcount = ping.get(session.getId());
                Integer pocount = pong.get(session.getId());

                if (pcount <= pocount) {
                    pcount++;
                    ping.put(session.getId(), pcount);

                    try {

                      session.getBasicRemote().sendPing(ByteBuffer.wrap("".getBytes()));
                    } catch (IllegalArgumentException | IOException e) {
                    }
                } else{
                        UserAndSocket andSocket = null;
                        if(GlobalVariable.user.containsKey(clientId)){
                            UserAndSocket userAndSocket = GlobalVariable.user.get(clientId);
                            System.out.println("Disconnected after no ping/pong recieve :" + userAndSocket.getUserId());
                            andSocket = userAndSocket;
                        }
                    
                        if(andSocket!=null){
                            GlobalVariable.user.remove(clientId);
                            andSocket.getTimer().cancel();
                            ping.remove(session.getId());
                            pong.remove(session.getId());
                        }
                    
                }
            }
        };
        connectionLostTimer1.scheduleAtFixedRate(connectionLostTimerTask1, 1 * 1000, 1 * 1000);
        return connectionLostTimer1;
    }
    private void sendBufferData(Session session,Integer clientId)
    {
        UserData user = new UserData();
        for(UserData userData: userRepository.findByUserId(clientId))
        {
            user = userData;
            if(userData!=null){
            try{       
                    String msgString="";
                    if(userData.getIsGroupMessage()){
                        if(null!=userData.getMessageType()){
                            
                            switch(userData.getMessageType()){
                                
                                case 2:{  //Create Group 
                                    
                                    msgString = "{\"response\":9,\"groupId\":" + userData.getGroupId() + ",\"creatorId\":" + userData.getSenderUserId() + ",\"msgBurn\":" + userData.getMsgBurn() + ",\"eccId\":\"" + userData.getEccId() + "\",\"mimeType\":" + userData.getMimeType() + ",\"screenName\":\"" + userData.getScreenName() + "\",\"name\":\"" + userData.getName() + "\",\"messageId\":\"" + userData.getMessageId() + "\",\"messsagestatus\":" + 0 + ",\"members\":" + userData.getMsgText()+ ",\"msgTex\":\"" + "" + "\"}";
                                    session.getBasicRemote().sendText(msgString);
                                    break;
                                }
                                
                                case 3:{  // Add member in Group
                                   msgString = "{\"response\":22,\"groupId\":" + userData.getGroupId() + ",\"creatorId\":" + userData.getSenderUserId() + ",\"msgBurn\":" + userData.getMsgBurn() + ",\"eccId\":\"" + userData.getEccId() + "\",\"mimeType\":" + userData.getMimeType() + ",\"screenName\":\"" + userData.getScreenName() + "\",\"name\":\"" + userData.getName() + "\",\"messageId\":\"" + userData.getMessageId() + "\",\"messsagestatus\":" + 0 + ",\"members\":" + userData.getMsgText() + ",\"msgTex\":\"" + "" + "\"}";
                                   session.getBasicRemote().sendText(msgString);
                                   break;
                                }
                                
                                case 4:{   //Delete member in Group
                                   msgString = "{\"response\":33,\"groupId\":" + userData.getGroupId() + ",\"creatorId\":" + userData.getSenderUserId() + ",\"msgBurn\":" + userData.getMsgBurn() + ",\"eccId\":\"" + userData.getEccId() + "\",\"mimeType\":" + userData.getMimeType() + ",\"screenName\":\"" + userData.getScreenName() + "\",\"name\":\"" + userData.getName() + "\",\"messageId\":\"" + userData.getMessageId() + "\",\"messsagestatus\":" + 0 + ",\"members\":" + userData.getMsgText() + ",\"msgTex\":\"" + "" + "\"}";
                                   session.getBasicRemote().sendText(msgString);
                                   break;
                                }
                                
                                case 5:{   // Messages in Group
                                    msgString ="{\"response\":8,\"groupId\":" + userData.getGroupId() + ",\"senderId\":"+ userData.getSenderUserId() + ",\"msgBurn\":"+ userData.getMsgBurn() + ",\"playSound\":" + 1 + ",\"eccId\":\"" + userData.getEccId() + "\",\"mimeType\":" + userData.getMimeType() + ",\"screenName\":\""+ userData.getScreenName() + "\",\"name\":\"" + userData.getName() + "\",\"messageId\":\"" + userData.getMessageId() + "\",\"messagestatus\":" + 0 + ",\"msgText\":\"" + userData.getMsgText() + "\",\"msgDateTime\":\"" + userData.getMessageDateTime() + "\"}";
                                    session.getBasicRemote().sendText(msgString);
                                    break;
                                }
                                
                                case 6:{  // Leave Member From Group
                                   msgString = "{\"response\":77,\"groupId\":" + userData.getGroupId() + ",\"creatorId\":" + userData.getSenderUserId() + ",\"eccId\":\"" + userData.getEccId() + "\",\"screenName\":\"" + userData.getScreenName() + "\",\"messageId\":\"" + userData.getMessageId() + "\",\"name\":\"" + userData.getName() + "\",\"messagestatus\":" + 0 + ",\"msgText\":\"" + userData.getMsgText() + "\",\"msgDateTime\":\"" + userData.getMessageDateTime() + "\"}" ;
                                   session.getBasicRemote().sendText(msgString);
                                   break;
                                }
                                
                                case 7:{   //  Update Group Name
                                    msgString ="{\"response\":99,\"groupId\":" + userData.getGroupId() + ",\"updatorId\":" + userData.getSenderUserId() + ",\"updateName\":\"" + userData.getName() + "\",\"messageId\":\"" + userData.getMessageId() + "\",\"messagestatus\":" + 0 + ",\"msgText\":\"" + "" + "\"}";
                                    session.getBasicRemote().sendText(msgString);
                                    break;
                                }
                            }
                        }
                    }
                   else if(null!=userData.getMessageType()){
                        switch(userData.getMessageType()){
                            
                            case 1: { // Single Chat
                                msgString = "{\"response\":2,\"sendFrom\":" +userData.getSenderUserId() + ",\"screenName\":"+ userData.getScreenName() + ",\"eccId\":" + userData.getEccId() +  ",\"sendTo\":" + clientId + ",\"msgDateTime\":" + userData.getMessageDateTime() + ",\"msgText\":" +userData.getMsgText() + ",\"msgVBurn\":" + userData.getMsgBurn() +  ",\"playSound\":1"  + ",\"mimeType\":" + userData.getMimeType() + ",\"messageStatus\":" + 0 + ",\"messageId\":" + userData.getMessageId() +",\"reply\":" +userData.getReply() + "}";
                                 session.getBasicRemote().sendText(msgString);
                                break;  
                            }
                            
                            case 8: {
                                msgString ="{\"response\":20,\"sendFrom\":" + userData.getSenderUserId() + ",\"sendTo\":" + clientId + ",\"messageId\":\"" + userData.getMessageId() + "\",\"messagestatus\":" + userData.getMessageStatus() + ",\"eccId\":\"" + userData.getEccId() + "\",\"screenName\":\"" + userData.getScreenName() + "\",\"reply\":\"" + "false" + "\"}";
                                session.getBasicRemote().sendText(msgString);
                                break;
                            }
                            
                        }
                    }
                   
                
            }catch (IOException ex) {   
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }   
        }     
    }           
}


