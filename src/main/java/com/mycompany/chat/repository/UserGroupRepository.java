/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.repository;

import com.mycompany.chat.web.UserGroup;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abc
 */
@Repository
public interface UserGroupRepository extends MongoRepository<UserGroup,String>{
    
    public UserGroup findUserGroupByGroupId(Integer groupId);
    
}
