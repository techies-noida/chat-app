/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.repository;

import com.mycompany.chat.web.UserData;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abc
 */
@Repository
public interface UserDataRepository extends MongoRepository<UserData,String> {

   public List<UserData> findByUserId(Integer userId);
   
   public String deleteUserDataByMessageId(String messageId, Integer userSendToId); 
   
   public boolean deleteDataByMessageId(String messageId);

    
}
