/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.server;

import java.util.HashMap;
import javax.websocket.Session;

/**
 *
 * @author abc
 */
public class UserGroupAndSocket implements Comparable<UserGroupAndSocket>{
    
    private Integer groupId;
    
    private Session socket;
    
    private HashMap<Integer,String> members;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Session getSocket() {
        return socket;
    }

    public void setSocket(Session socket) {
        this.socket = socket;
    }

    public HashMap<Integer, String> getMembers() {
        return members;
    }

    public void setMembers(HashMap<Integer, String> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "UserGroupAndSocket{" + "groupId=" + groupId + ", socket=" + socket + ", members=" + members + '}';
    }

    @Override
    public int compareTo(UserGroupAndSocket o) {
        if(o.getGroupId()> groupId)
            return -1;
        else
            return 1;     
    }
    
    
    
    
    
}
