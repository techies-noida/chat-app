/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.server;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author abc
 */
public class GlobalVariable {
    
    //public static Set<UserAndSocket> userAndSocket = Collections.synchronizedSet(new LinkedHashSet<UserAndSocket>());
    public static Map<Integer, UserAndSocket> user = Collections.synchronizedMap(new HashMap<Integer, UserAndSocket>());

    public static Set<UserGroupAndSocket> userGroupAndSocket = Collections.synchronizedSet(new LinkedHashSet<UserGroupAndSocket>());

}

