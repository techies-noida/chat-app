/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.chat.server;

import java.util.Objects;
import java.util.Timer;
import javax.websocket.Session;

/**
 *
 * @author abc
 */
public class UserAndSocket implements Comparable<UserAndSocket>{
    
    private Integer userId;
    
    private Session socket;
    
    private Timer timer;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Session getSocket() {
        return socket;
    }

    public void setSocket(Session socket) {
        this.socket = socket;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    @Override
    public String toString() {
        return "UserAndSocket{" + "userId=" + userId + ", socket=" + socket + ", timer=" + timer + '}';
    }
    
    @Override
    public int compareTo(UserAndSocket o) {
        if(o.getUserId()>userId)
            return -1;
        else
            return 1;
    }
    
     @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.userId);
         System.out.println("hash code is "+hash);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final UserAndSocket other = (UserAndSocket) obj;
//        if (!Objects.equals(this.userId, other.userId)) {
//            return false;
//        }
//        return true;
            return false;
    }

}
